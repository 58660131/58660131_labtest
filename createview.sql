CREATE VIEW Registration AS
SELECT s.StudentID AS StudentID, s.StudentName AS StudentName,
	s.StudentSurname AS StudentSurname,
	c.CourseName AS CourseName, c.Credit AS Credit, r.Semester AS Semester
FROM Student AS s
LEFT JOIN (Regist AS r, Courses AS c)
ON (r.CourseID=c.CourseID AND r.StudentID=s.StudentID);
